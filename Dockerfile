#Build Stage
FROM node:18-alpine AS BUILD_IMAGE
WORKDIR /app
COPY package*.json ./
RUN npm install -g npm@10.2.4
RUN npm ci
COPY . .
RUN npm run build

##
## Deploy
##
FROM node:18-alpine AS PRODUCTION_STAGE

WORKDIR /app

COPY --from=BUILD_IMAGE /app/.next ./.next
COPY --from=BUILD_IMAGE /app/public ./public
COPY --from=BUILD_IMAGE /app/package.json .
COPY --from=BUILD_IMAGE /app/node_modules ./node_modules

RUN npm install
EXPOSE 3000

CMD ["npm", "start"]