import 'dotenv/config'
import { z } from 'zod'

const envSchema = z.object({
  CLIENTID: z.string(),
  CLIENTSECRET: z.string(),
  TOKENURL: z.string(),
  RESOURCE: z.string(),
  TENANTID: z.string(),
  USERNAME: z.string(),
  PASSWORD: z.string(),
})

const _env = envSchema.safeParse(process.env)

if (_env.success === false) {
  console.error('Invalid environment variables', _env.error.format())

  throw new Error('Invalid environment variables')
}

export const env = _env.data
