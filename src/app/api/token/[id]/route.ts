import { NextRequest, NextResponse } from "next/server"
import axios from 'axios';
import { env } from '@/env'

const token_url = `https://login.microsoftonline.com/${env.TENANTID}/oauth2/token`;

async function getTokenWithPassword (token_url: string) {

    const requestBody = {
        grant_type: 'password',
        scope: 'openid',
        client_id: '8fdf9803-908d-4f01-bc4e-bf6b112968fb',
        client_secret: 'kRI8Q~TpUI5xAdmS.an6lgrZ-MtquAV458wRxbwU',
        resource: 'https://analysis.windows.net/powerbi/api',
        username: 'powerbi_automation@magalu-azure.com.br',
        password: '=CncfO44OYXYwR}Fg5i-'
    };

    const requestHeaders = {
          'Content-Type': 'application/x-www-form-urlencoded'
    };

    try {
        const response = await axios.post(token_url, requestBody, { headers: requestHeaders });
        return response.data.access_token;
      } catch (error) {
        console.error('Erro ao obter o token:', error);
      }

}

export async function GET(req: NextRequest) {
    const params = new URLSearchParams(req.url).getAll('id');

    try {

        const userToken = await getTokenWithPassword(token_url);

        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${userToken}`,
        };

        const apiUrl = `https://api.powerbi.com/v1.0/myorg/groups/1b6db161-b03b-491e-82c2-3cab755e1fd7/reports/${params[0]}`
        const response = await axios.get(apiUrl, { headers })

        const body = {
            'request': apiUrl
        }


        const requestHeaders = {
            'Authorization': `Bearer ${userToken}`,
        };

        const generateTokenResponse = await axios.post(`${apiUrl}/GenerateToken`, body,{ headers: requestHeaders })

        return NextResponse.json({ access_token: generateTokenResponse.data.token, response: response.data });
    } catch (error) {
        return NextResponse.json({ error: 'Failed to get access token', status: 500 });
    }
}