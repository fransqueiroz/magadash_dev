'use client';
import { ReactNode, createContext, useState } from "react";

interface NavbarButtonContextProps {
  isOpen: boolean
  handleButton: () => void
}

interface NavbarButtonContextProviderProps {
  children: ReactNode
}

const NavbarButtonContext = createContext<NavbarButtonContextProps>({
  isOpen: true,
  handleButton: () => {}
})

const NavbarButtonContextProvider = ({children}: NavbarButtonContextProviderProps) => {
    const [isOpen, setIsOpen] = useState(true);

    const handleButton = () => {
      setIsOpen(!isOpen)
    }

    return (
      <NavbarButtonContext.Provider value={{isOpen, handleButton}}>
        {children}
      </NavbarButtonContext.Provider>
    )
}

export {NavbarButtonContext, NavbarButtonContextProvider};