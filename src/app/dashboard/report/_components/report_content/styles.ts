import styled from "styled-components";

interface NavbarColapseButtonProps {
    variant: boolean
}



export const ReportContentContainer = styled.main<NavbarColapseButtonProps>`

    padding-left: ${props => props.variant? '15rem' : '6rem'};
    width:100%;
    transition: padding 0.2s ease-in-out;

    .report-container{
        width:100%;
        height:100vh;
    }
`;
