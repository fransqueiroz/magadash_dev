import { NavbarButtonContext } from "@/app/dashboard/context/IsOpenContext";
import { useContext, useEffect, useState } from "react";
import { PowerBIEmbed } from "powerbi-client-react";
import axios from "axios";
import { models, Report, Embed, service, Page } from 'powerbi-client';
import { IHttpPostMessageResponse } from 'http-post-message';
import { ReportContentContainer } from "./styles";

const ReportContent = ({ report_id }: { report_id: string }) => {
  const {isOpen } = useContext(NavbarButtonContext);
  const [report, setReport] = useState<Report>();

	// Track Report embedding status
	const [isEmbedded, setIsEmbedded] = useState<boolean>(true);

    // Overall status message of embedding
	const [displayMessage, setMessage] = useState(`The report is bootstrapped. Click the Embed Report button to set the access token`);

	// CSS Class to be passed to the embedded component
	const reportClass = 'report-container';

	// Pass the basic embed configurations to the embedded component to bootstrap the report on first load
    // Values for properties like embedUrl, accessToken and settings will be set on click of button
	const [sampleReportConfig, setReportConfig] = useState<models.IReportEmbedConfiguration>({
		type: 'report',
		embedUrl: undefined,
		tokenType: models.TokenType.Embed,
		accessToken: undefined,
		settings: undefined,
	});

  const[eventHandlersMap, setEventHandlersMap] = useState<Map<string, (event?: service.ICustomEvent<any>, embeddedEntity?: Embed) => void | null>>(new Map([
		['loaded', () => console.log('Report has loaded')],
		['rendered', () => console.log('Report has rendered')],
		['error', (event?: service.ICustomEvent<any>) => {
				if (event) {
					console.error(event.detail);
				}
			},
		],
		['visualClicked', () => console.log('visual clicked')],
		['pageChanged', (event) => console.log(event)],
	]));


  const embedReport = async (): Promise<void> => {
		console.log('Embed Report clicked');

		// Get the embed config from the service
		const reportConfigResponse = await axios.get(`http://localhost:3000/api/token/${report_id}`);

		// Update the reportConfig to embed the PowerBI report
		setReportConfig({
			...sampleReportConfig,
			embedUrl: reportConfigResponse.data.response.embedUrl,
			accessToken: reportConfigResponse.data.access_token
		});
		setIsEmbedded(true);

		// Update the display message
		setMessage('Use the buttons above to interact with the report using Power BI Client APIs.');
	};

  const hideFilterPane = async (): Promise<IHttpPostMessageResponse<void> | undefined>  => {
		// Check if report is available or not
		if (!report) {
			return;
		}

		// New settings to hide filter pane
		const settings = {
			panes: {
				filters: {
					expanded: false,
					visible: false,
				},
			},
		};

		try {
			const response: IHttpPostMessageResponse<void> = await report.updateSettings(settings);

			// Update display message
			return response;
		} catch (error) {
			console.error(error);
			return;
		}
	};

  const setDataSelectedEvent = () => {
		setEventHandlersMap(new Map<string, (event?: service.ICustomEvent<any>, embeddedEntity?: Embed) => void | null> ([
			...eventHandlersMap,
			['dataSelected', (event: any) => console.log(event)],
		]));

		setMessage('Data Selected event set successfully. Select data to see event in console.');
	}



  useEffect(() => {
		if (report) {
			report.setComponentTitle('Embedded Report');
		}

    embedReport();
	}, [report]);


  const reportComponent =
		<PowerBIEmbed
			embedConfig = { sampleReportConfig }
			eventHandlers = { eventHandlersMap }
			cssClassName = { reportClass }
			getEmbeddedComponent = { (embedObject: Embed) => {
				console.log(`Embedded object of type "${ embedObject.embedtype }" received`);
				setReport(embedObject as Report);
			} }
		/>;

    return (
      <ReportContentContainer variant={isOpen}>
        { isEmbedded ? reportComponent : null }
      </ReportContentContainer>
    );

}

export default ReportContent;
