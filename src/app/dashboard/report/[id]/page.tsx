'use client';
import { ReportContainer } from "./styles";
import { NavbarButtonContextProvider } from "../../context/IsOpenContext";
import Navbar from "../../components/Navbar";
import ReportContent from '../_components/report_content';

export default function Report({params}: {
    params: {id: string}
}) {

    return (
        <ReportContainer>
            <NavbarButtonContextProvider>
                <Navbar/>
                <ReportContent report_id={params.id}/>
            </NavbarButtonContextProvider>
        </ReportContainer>
    )
}