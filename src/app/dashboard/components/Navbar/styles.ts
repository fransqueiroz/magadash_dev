import Link from "next/link";
import styled from "styled-components";

interface NavbarColapseButtonProps {
    variant: boolean
}

interface NavbarMenuListProps {
    variant: boolean
    active: boolean
}

export const NavbarContainer = styled.nav<NavbarColapseButtonProps>`
    width: ${props => props.variant? '15rem' : '6rem'};
    height:100%;

    display:flex;
    flex-direction:column;
    z-index: 50;

    position: fixed;
    top: 0;
    bottom: 0;

    background: #fff;

    box-shadow: 3px 0px 8px -3px rgba(0,77,145,0.75);
    -webkit-box-shadow: 3px 0px 8px -3px rgba(0,77,145,0.75);
    -moz-box-shadow: 3px 0px 8px -3px rgba(0,77,145,0.75);
    transition: all 0.2s ease-in-out;

`;

export const NavbarCollapseButton = styled.button`
    background:transparent;
    border:0;
    position:absolute;
    top:5px;
    right:-5px;
    color:#0086ff;
    transition: all 0.2s ease-in-out;
`;

export const NavbarHeader = styled.header`
    display:flex;
    align-items:center;
    justify-content:center;
    height: 80px;
    position:relative;

    padding-left: 1.5rem;
    padding-right: 1.5rem;

    border-bottom-width: 1px;
    border-color: rgba(0,77,145,0.75);
    border-style: solid;

    p{
        font-size:2rem;
        font-weight:700;
        color:#0086ff;
    }
`;

export const NavbarMenuContainer = styled.div<NavbarColapseButtonProps>`
    padding: ${props => props.variant? '1.5rem' : '0.5rem'} ;
`;

export const NavbarMenu = styled.ul`
    margin-top:1rem;
`;

export const NavbarMenuList = styled.li<NavbarMenuListProps>`
`;

export const NavbarMenusLink = styled(Link)<NavbarMenuListProps>`
    text-decoration:none;
    display:flex;
    align-items:center;
    justify-content: ${props => props.variant? 'flex-start' : 'center'} ;
    font-size:1rem;
    line-height: 1.5rem;
    font-weight:700;
    -webkit-line-clamp: 2;
    color: ${props => props.active? '#fff' : '#0086ff'};

    background: ${props => props.active? '#0086ff' : '#fff'};
    padding: 1rem;
    border-radius:6px;
    transition: all 0.3s ease-in-out;
`;
