'use client';
import React, { useContext, useState } from 'react';

import {
  NavbarContainer,
  NavbarHeader,
  NavbarCollapseButton,
  NavbarMenuContainer,
  NavbarMenu,
  NavbarMenuList,
  NavbarMenusLink
} from './styles';

import {
  CaretCircleDoubleLeft,
  CaretCircleDoubleRight,
  House
} from 'phosphor-react';
import { NavbarButtonContext } from '@/app/dashboard/context/IsOpenContext';

export default function Navbar() {


  const {isOpen, handleButton } = useContext(NavbarButtonContext)
  const [activePage, setActivePage] = useState<boolean>(true)

  return (
    <NavbarContainer variant={isOpen}>
      <NavbarHeader>
        <NavbarCollapseButton onClick={handleButton}>
          {isOpen ? (
            <CaretCircleDoubleLeft size={24} />
          ) : (
            <CaretCircleDoubleRight size={24} />
          )}
        </NavbarCollapseButton>
        {isOpen ? (
          <p>MagaDash</p>
        ) : (
          <p>Maga</p>
        )}
      </NavbarHeader>
      <NavbarMenuContainer variant={isOpen}>
          <NavbarMenu>
            <NavbarMenuList variant={isOpen} active={activePage}>
              <NavbarMenusLink
                variant={isOpen}
                active={activePage}
                href={`/dashboard/home`}
              >
                <House size={24}/>
                {isOpen ? (
                    <>Home</>
                  ) : (
                    <></>
                )}
              </NavbarMenusLink>
            </NavbarMenuList>
          </NavbarMenu>
      </NavbarMenuContainer>
    </NavbarContainer>
  );
}