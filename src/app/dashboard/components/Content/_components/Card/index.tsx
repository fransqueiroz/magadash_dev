import Image from "next/image";
import { CardContainer, CardContent } from "./style";
import ImageBFTV from '@/app/assets/dash_bf_tv.png';
import ImageBFOnePage from '@/app/assets/dash_bf_onpage.png';
import Link from "next/link";
import { useRouter } from "next/navigation";

export default function Card(
    {
        title,
        report_id,
        owner,
        update
    }: {
        title:string,
        report_id: string,
        owner: string,
        update: string
    }) {

    const { push } = useRouter();

    return (
        <>

            <CardContainer>
                <CardContent onClick={() => push(`/dashboard/report/${report_id}`)}>
                    <div className="image_content">
                        <Image src={ImageBFOnePage} alt="" />
                    </div>
                    <div className="info_content">
                        <div className="info_content_title">
                            <h2 className="title">{title}</h2>
                        </div>
                        <div className="info_content_owner">
                            <p>Owner: {owner}</p>
                        </div>
                        <div className="info_content_updated">
                            <p>Atualizado: {update}</p>
                        </div>
                        <div className="info_content_button">
                            <Link className="info_content_button_link" href={`/dashboard/report/${report_id}`}>Acessar</Link>
                        </div>
                    </div>
                </CardContent>
            </CardContainer>

        </>

    )
}