import Link from "next/link";
import styled from "styled-components";

export const CardContainer = styled.div`
    width:100%;
    max-width: 28rem;
    position: relative;
    min-height: 1px;
    padding:0.5rem;
    margin:1rem 0;
`;

export const CardContent = styled.div`

    padding: 0.75rem;
    border-radius:0.5rem;
    overflow: hidden;
    display:flex;
    flex-direction:column;
    cursor: pointer;

    border: 1px solid #e5e7eb;

    &:hover{
        box-shadow: -1px -1px 12px 1px rgba(0,0,0,0.3);
        -webkit-box-shadow: -1px -1px 12px 1px rgba(0,0,0,0.3);
        -moz-box-shadow: -1px -1px 12px 1px rgba(0,0,0,0.3);
    }

    .image_content{

        max-width: 28rem;
        max-height: 17rem;
        overflow: hidden;

        img {
            object-fit: cover;
            height:100%;
            width:100%;
        }
    }

    .info_content{

        display:flex;
        flex-direction:column;
        padding-top:0.5rem;

        .info_content_title{
            font-size:1rem;
            line-height: 1.5rem;
            font-weight:500;
            -webkit-line-clamp: 2;
            color:#0086ff;
        }

        .info_content_owner,
        .info_content_updated{
            font-size: .75rem;
            line-height: 1rem;
            color: #8f8f8f;
        }

        .info_content_button{
            margin-top:0.5rem;

            .info_content_button_link{
                text-decoration:none
            }
        }
    }
`;