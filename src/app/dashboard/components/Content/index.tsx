import { PageContent, PageContainer, PageRow } from "./styles";
import { NavbarButtonContext } from '@/app/dashboard/context/IsOpenContext';
import { useContext } from "react";
import Card from "./_components/Card";

export default function Content() {

    const {isOpen } = useContext(NavbarButtonContext)

    return (
        <PageContent variant={isOpen}>
            <PageContainer>
                <PageRow>
                    <Card
                        title="Dash BF One Page"
                        report_id="00076b79-f041-4d18-8b0a-399ad6039290"
                        owner="Eduardo Lima"
                        update="10/11/2023"
                    />
                    <Card
                        title="Dash BF TV"
                        report_id="00076b79-f041-4d18-8b0a-399ad6039290"
                        owner="Eduardo Lima"
                        update="10/11/2023"
                    />
                </PageRow>
            </PageContainer>
        </PageContent>
    )
}