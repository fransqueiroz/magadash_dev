import styled from "styled-components";

interface NavbarColapseButtonProps {
    variant: boolean
}

export const PageContent = styled.div<NavbarColapseButtonProps>`

    padding-left: ${props => props.variant? '15rem' : '6rem'};
    width:100%;
    transition: padding 0.2s ease-in-out;
`;

export const PageContainer = styled.div`
    padding: 0 1rem;
`;

export const PageRow = styled.div`
    display:flex;
    flex-wrap: wrap;
    padding: 2rem;
    justify-content:flex-start
`;