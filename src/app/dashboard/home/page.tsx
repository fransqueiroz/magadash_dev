'use client';
import Navbar from '@/app/dashboard/components/Navbar'
import { PageContainer } from './styles'
import { NavbarButtonContextProvider} from '../context/IsOpenContext'
import { useContext } from 'react';
import Content from '../components/Content';

export default function DashBoard() {

  return (
    <PageContainer>
      <NavbarButtonContextProvider>
          <Navbar/>
          <Content/>
      </NavbarButtonContextProvider>
    </PageContainer>
  )
}
