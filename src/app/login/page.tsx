'use client';
import { z } from "zod";
import {
    PageLoginCard,
    PageLoginContainer,
    PageLoginHeader,
    PageLoginForm,
    PageLoginInputContainer,
    PageLoginButton
} from "./style";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { useEffect, useState } from "react";
import { redirect } from "next/navigation";

const loginUserFormSchema = z.object({
    user: z.string(),
    password: z.string()
})

type NewLoginUserFormInputs = z.infer<typeof loginUserFormSchema>

export default function DashBoard() {
    const [errorUser, setErrorUser] = useState('')
    const [errorPassword, setErrorPassword] = useState('')

    const {
        register,
        handleSubmit,
        formState: {isSubmitting},
    } = useForm<NewLoginUserFormInputs>({
        resolver: zodResolver(loginUserFormSchema)
    })

    const [redirectToHome, setRedirectToHome] = useState(false);

    async function handleLoginUser(data: NewLoginUserFormInputs) {
        const {user, password} = data;
        setErrorUser('');
        setErrorPassword('');

        if (!user) {
            setErrorUser("Digite seu usuário");
        }

        if (!password) {
            setErrorPassword("Digite sua senha");
        }

        if (user !== 'admin') {
            setErrorUser("Usuário não encontrado");
        }

        if (password != 'Mdbf_2023') {
            setErrorPassword("Senha Incorreta");
        }

        if (user === 'admin' && password === 'Mdbf_2023'){
            setRedirectToHome(true);
        }

    }

    useEffect(() => {
        if (redirectToHome) {
          redirect('/dashboard/home');
        }
      }, [redirectToHome]);

    return(
        <PageLoginContainer>
            <PageLoginCard>
                <PageLoginHeader>
                    <h1>MagaDash</h1>
                </PageLoginHeader>
                <PageLoginForm onSubmit={handleSubmit(handleLoginUser)}>
                    <PageLoginInputContainer>
                        <input
                            type="text"
                            required
                            {...register('user')}
                        />
                        <label className="label-10">
                            <span className="label-text-10">Digite seu Usuário </span>
                        </label>
                        {errorUser != ''? (
                            <span className="error-message">{errorUser}</span>
                        ): (<></>)}
                    </PageLoginInputContainer>
                    <PageLoginInputContainer>
                        <input
                            type="password"
                            required
                            {...register('password')}
                        />
                        <label className="label-10">
                            <span className="label-text-10">Digite sua Senha </span>
                        </label>
                        {errorPassword != ''? (
                            <span className="error-message">{errorPassword}</span>
                        ): (<></>)}
                    </PageLoginInputContainer>

                    <PageLoginButton type="submit" disabled={isSubmitting}>
                        Continuar
                    </PageLoginButton>
                </PageLoginForm>
            </PageLoginCard>
        </PageLoginContainer>
    )
}