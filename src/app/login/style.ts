import styled from "styled-components";

export const PageLoginContainer = styled.main`
    display:flex;
    flex-wrap:wrap;
    justify-content:center;
    align-items:center;
    background-size: cover;
    background-color: #ededed;
    width:100vw;
    height:100vh;
`;

export const PageLoginHeader = styled.header`
    padding-bottom: 5rem;

    h1{
        font-size:5rem;
        font-weight:bold;
        color:#0086ff;
    }
`;

export const PageLoginCard = styled.div`

    padding: 2rem 6rem 3.75rem!important;
    background: #fff;
    margin: 0 auto;
    padding: 0 20px;
    width:100%;
    max-width: 500px;
    height:100%;
    max-height:530px;
    border-top: 0;
    box-shadow: 0 0 0;
    border-radius: 10px;

    display:flex;
    align-items:center;
    justify-content:center;
    flex-direction:column;

`;

export const PageLoginForm = styled.form`
    display:flex;
    align-items:center;
    flex-direction:column;
    justify-content:center;
    gap: 1rem;
    width:100%;
`;

export const PageLoginInputContainer = styled.div`


    position: relative;
    overflow: hidden;
    padding-bottom: 5px;
    width:100%;
    margin-bottom:1rem;


    input {
        padding-top: 20px;
        padding-bottom: 5px;
        border: 1px solid #fff;
        color: #595f6e;
        font-size: 1rem;
    }


    label {
        position: absolute;
        bottom: 0px;
        left: 0;
        height: 100%;
        width: 100%;
        pointer-events: none;
        border-bottom: 1.5px solid #595f6e;
    }

    label::after {
        content: "";
        position: absolute;
        left: 0;
        bottom: -1px;
        height: 100%;
        width: 0%;
        border-bottom: 6px solid rgba(0,0,0,0.1);
        left: 50%;
        transition: all 0.4s ease;
        z-index:40;
    }

    label .label-text-10 {
        position: absolute;
        bottom: 5px;
        left: 0;
        transition: all 0.3s ease;
        opacity: 0.5;
        font-size: 1rem;
        z-index:40;
    }

    input:focus + .label-10 .label-text-10,
    input:valid + .label-10 .label-text-10 {
        transform: translateY(-200%);
        font-size: 14px;
        font-weight: bold;
        color: rgba(0,0,0,0.1);
        opacity: 1;
        z-index:40;
    }

    input:focus + .label-10::after,
    input:valid + .label-10::after {
        left: 0;
        width: 100%;
        transition: all 0.4s ease;
        z-index:40;

    }

    input:focus-visible{
        border:0;
        outline:0;
    }
`;

export const PageLoginButton = styled.button`

    width:100%;
    height: 58px;
    border:0 ;
    background: #0086ff;
    color: #fff;
    font-weight: bold;
    padding: 0 1.25rem;
    border-radius: 46px;
    margin-top:1.5rem;
    cursor: pointer;
    text-decoration:none;
    transition: background-color 0.2s;

    display: flex;
    align-items:center;
    justify-content:center;

    font-size: 1rem;
    font-weight: 400;

    &:hover{
        opacity:.6;
    }

`;